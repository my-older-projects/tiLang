/* eslint max-len: 0 */

'use strict';
// Utilities
const delay = (time, cb) => setTimeout(cb, time);
const deepCopy = obj => JSON.parse(JSON.stringify(obj));
const noop = () => {};

const mathjs = require('mathjs');

class Utils {
	static createNewScope(superScope) {
		return [{}, superScope];
	}

	static saveToScope(isNew, variable, value, scope, globalScope) {
		let dollarVar = '${'.concat(variable).concat('}');

		if (isNew) {
			scope[0][dollarVar] = value;
			return;
		}

		let varScope = Utils.scopeLookup(dollarVar, scope);
		if (varScope) {
			varScope[dollarVar] = value;
		} else {
			// undefined, we need to write to the global
			Utils.setVariable(true, variable, value, globalScope, globalScope);
		}
	}

	static createVariable(value) {
		return {
			type: (typeof value),
			content: value
		};
	}

	static createFunc(args, array) {
		return {
			type: 'func',
			content: array,
			argNames: args
		};
	}

	static setVariable(isNew, variable, value, scope, globalScope) {
		if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
			Utils.saveToScope(isNew, variable, Utils.createVariable(value), scope, globalScope);
		} else if (value.type) {
			// zaten bir type var yani belirlenmiş direk ver gitsin
			Utils.saveToScope(isNew, variable, value, scope, globalScope);
		} else {
			Utils.saveToScope(isNew, variable, {
				type: 'unknown',
				content: value
			}, scope, globalScope);
		}
	}

	static setFunc(name, args, array, scope) {
		Utils.saveToScope(true, name, Utils.createFunc(args, array), scope);
	}

	static getVariable(variable, scope, abortOnError) {
		let varScope = Utils.scopeLookup(variable, scope);
		if (varScope) {
			return varScope[variable];
		}

		if (abortOnError) {
			throw new Error('ReferenceError: Unknown Variable');
		}
		return {
			type: 'undefined',
			content: undefined
		};
	}

	static getFunc(name, scope, abortOnError) {
		let dollarName = '${'.concat(name).concat('}');
		return Utils.getVariable(dollarName, scope, abortOnError);
	}

	static scopeLookup(variable, scope) {
		let lookup = (vari, skop) => {
			if (skop[0][vari]) {
				return skop[0];
			}

			if (typeof skop[1] === 'undefined') {
				// reached global and it isnt there either
				return undefined;
			}

			return lookup(vari, skop[1]);
		};

		return lookup(variable, scope);
	}

	static removeStringSigns(string) {
		if (string.charAt(0) === '"' || string.charAt(0) === "'") {
			string = string.slice(0, 0) + string.slice(1);
		}
		let strlast = string.length - 1;
		if (string.charAt(strlast) === '"' || string.charAt(strlast) === "'") {
			string = string.slice(0, strlast) + string.slice(strlast + 1);
		}

		return string;
	}
}

class Argument {
	constructor(execRefer, scope, arg, callback) {
		this.execRefer = execRefer;

		this.scope = scope;
		this.arg = arg;
		this.callback = callback;
	}

	process() {
		// eğer subsheller varsa içinde.
		if (this.arg.SHELLS) {
			let i = 0;
			let continuation = returnedShellValue => {
				if (typeof returnedShellValue === 'string') {
					returnedShellValue = Utils.removeStringSigns(returnedShellValue);
				}
				this.arg.SHELLS[i] = returnedShellValue;

				i++;
				if (this.arg.SHELLS[i]) {
					this.processShell(i, continuation);
					return;
				}
				// tüm shells arayı elemanları bitti, eğer birden fazlaysa, birleştirek
				if (this.arg.SHELLS.length > 1) {
					this.arg = this.arg.SHELLS.join('');
				} else {
					this.arg = this.arg.SHELLS[0];
				}
				this.postProcess();

				this.callback(this.arg);
			};
			this.processShell(i, continuation);
			return;
		}

		// subshell falan yok, bildiğimiz tertemiz bu argument sadece variable yerleştirmek lazım

		this.postProcess();
		this.callback(this.arg);
	}

	processShell(i, continuation) {
		let shells = this.arg.SHELLS;
		if (shells[i].SHELL) {
			let block = new Block(this.execRefer, shells[i].SHELL, 'subShell', {superScope: this.scope}, (flowChange, returnedValue) => { // eslint-disable-line no-use-before-define
				Block.flowStandards(flowChange, returnedValue, noop, (flowChange, returnedValue) => { // eslint-disable-line no-use-before-define
					continuation(returnedValue);
				});
			});
			block.execute();
			return;
		}
		// bir shell diilmiş direk kendisini dönelim
		continuation(shells[i]);
	}

	postProcess() {
		if (typeof this.arg === 'string') {
			if (this.arg.includes(this.execRefer.variableSign)) {
				this.arg = this._placeVariables(this.arg);
			} else {
				this.arg = Utils.removeStringSigns(this.arg);
			}
		}

		// fixme String-to-Number fixe gerek yok ama parser da yapmak isterim bunu
		if (isFinite(this.arg) && this.arg !== '') {
			this.arg = parseFloat(this.arg);
		}
	}

	_placeVariables(string) {
		// giren şey: 5+$b
		// yada       5+$b+6
		// yadaaa     $b
		//
		// eğer son hali gibiyse sakın bunları stringlermiş gibi birleştirme direk dön
		let matches = string.match(this.execRefer.variableRegex);
		let others = string.split(this.execRefer.variableRegex);
		let result = [];
		let conMatches = [];
		// yerleştir
		for (let i = 0; i < matches.length; i++) {
			conMatches[i] = this._getVariable(matches[i]);
			if (conMatches[i].type !== 'func') {
				// fonksiyonsan olduğundun gibi kal
				conMatches[i] = conMatches[i].content;
			}
			// sıraya diz
			result.push(others[i]);
			result.push(conMatches[i]);
		}
		// sonuncu artanı da koy
		result.push(others.pop());
		// boş stringleri çıkar
		for (let i = 0; i < result.length; i++) {
			if (result[i] === '') {
				result.splice(i, 1);
				i--;
				continue;
			}

			if (typeof result[i] === 'string') {
				result[i] = Utils.removeStringSigns(result[i]);
			}
		}

		// eğer result 1 tane 'şey' (string demiyorum belki fonksiyon) oluşuosa join yapmadan direkt çık
		if (result.length === 1) {
			return result.pop();
		}

		return result.join('');
	}

	_getVariable(variable) {
		return Utils.getVariable(variable, this.scope, this.execRefer.abortOnError);
	}
}

class Line {
	constructor(execRefer, scope, line, callback) {
		this.execRefer = execRefer;

		this.scope = scope;
		this.line = line;
		this.callback = callback;
	}

	/**
	 * @recursive
	 * @async
	 */
	execute() {
		delay(this.execRefer.sleepTime, () => {
			this.execRefer.sleepTime = 0;

			this.processArgs(() => {
				// add executing next command to the callback chain
				this.line.ARGS.push(this.callback);
				this._execute();
			});
		});
	}

	processArgs(callback) {
		/* Put Values into Variables
		 * Logic
		 * Convert NumberStrings into Numbers
		 */
		if (typeof this.line.ARGS === 'undefined' || this.line.ARGS.length === 0) {
			// eğer hiç arg yoksa, mesela loneScope, direk callback yap ve çık veya arg sayısı 0 sa çık
			this.line.ARGS = [];
			callback();
			return;
		}

		let i = 0;
		let argCallback = processedArg => {
			this.line.ARGS[i] = processedArg;

			i++;
			if (this.line.ARGS[i]) {
				let arg = new Argument(this.execRefer, this.scope, this.line.ARGS[i], argCallback);
				arg.process();

				return;
			}
			// tüm arglar processlendi _execute devresine devam et
			callback();
		};
		let arg = new Argument(this.execRefer, this.scope, this.line.ARGS[i], argCallback);
		arg.process();

		return;
	}

	_execute() {
		/* Order of Operations
		 * 1) Executer's Functions
		 * 2) Custom Functions
		 * 3) Functions
		 * 4) Variables
		 * 4) ERROR
		 */

		try {
			if (this.execRefer.builtInFunctions()[this.line.COMMAND]) {
				// eğer if gibi postprocesse uğramışsa packı olabilir, ona göre davran
				// callback bi array dikkat
				let callback = [this.line.ARGS.pop()];
				if (typeof this.line.PACK === 'undefined') {
					this.line.PACK = [];
				}
				let together = this.line.ARGS.concat(this.line.PACK, callback);

				this.execRefer.builtInFunctions()[this.line.COMMAND].apply(this, together);
			} else if (this.execRefer.customFunctions[this.line.COMMAND]) {
				this.execRefer.customFunctions[this.line.COMMAND].apply(this, this.line.ARGS);
			} else if (this._getFunc(this.line.COMMAND).type === 'func') {
				// construct scope of the function
				let scope = Utils.createNewScope(this.scope);
				// Add Args of function as variables
				// -1 because I dont want to include cb
				for (let i = 0; i < this.line.ARGS.length - 1; i++) {
					Utils.setVariable(true, this._getFunc(this.line.COMMAND).argNames[i], this.line.ARGS[i], scope);
				}

				let block = new Block(this.execRefer, deepCopy(this._getFunc(this.line.COMMAND).content), 'funcBlock', {scope: scope}, (flowChange, returnedValue) => { // eslint-disable-line no-use-before-define
					// Burada senderCallback'e noop verdim çünkü bu function çıkışı olduğundan ona asla 'recall' gelmicek
					Block.flowStandards(flowChange, returnedValue, noop, () => { // eslint-disable-line no-use-before-define
						// buraya returned koydum çünküü return valini geri dönmemiz lazım dimi
						(this.line.ARGS.pop())(undefined, returnedValue);
					});
				});
				block.execute();
			} else {
				console.log(`ReferenceError: ${this.line.COMMAND} can't be found`);
				if (this.execRefer.abortOnError) {
					throw new Error('ReferenceError');
				}
				// after error do the callback
				(this.line.ARGS.pop())();
			}
		} catch (error) {
			console.log('RuntimeError: ', error);
			if (this.execRefer.abortOnError) {
				throw error;
			}
			// after error do the callback
			(this.line.ARGS.pop())();
		}
	}

	_setVariable(isNew, variable, value) {
		Utils.setVariable(isNew, variable, value, this.scope, this.execRefer.globalScope);
	}

	_setFunc(name, args, array) {
		Utils.setFunc(name, args, array, this.scope);
	}

	_getVariable(variable) {
		return Utils.getVariable(variable, this.scope, this.execRefer.abortOnError);
	}

	_getFunc(name) {
		return Utils.getFunc(name, this.scope, this.execRefer.abortOnError);
	}
}

class Block {
	constructor(execRefer, block, type, scopeOptions, callback) {
		this.execRefer = execRefer;
		this.type = type;

		this.block = block;
		this.scope = scopeOptions.superScope ? [{}, scopeOptions.superScope] : scopeOptions.scope;
		this.callback = callback;
	}

	execute() {
		let line = new Line(this.execRefer, this.scope, this.block.shift(), (flowChange, returnedValue) => {
			if (flowChange === 'skip') {
				// skip geldi fordan çık
				if (this.type === 'forBlock') {
					this.callback();
					return;
				}

				this.callback(flowChange);
				return;
			}

			if (flowChange === 'jump') {
				// jump geldi fordan çık, ama eğer bir for bloğuysan fora söyle o da dursun.
				if (this.type === 'forBlock') {
					this.callback(flowChange);
					return;
				}

				this.callback(flowChange);
				return;
			}

			if (flowChange === 'recall') {
				// recall geldi çık ama bir fonksiyona gelene kadar diğer bloklara da çıkmalarını söyle. Fonksiyon gelince ondanda çık ama ondan sonra çıkmaya gerek yok
				if (this.type === 'funcBlock' || this.type === 'mainBlock' || this.type === 'subShell') {
					this.callback(undefined, returnedValue);
					return;
				}

				this.callback(flowChange, returnedValue);
				return;
			}

			// no flow change

			if (this.block.length === 0) {
				// blok bitti normal çık
				// returnedValueyi koydum buraya çünkü bir func bitince callbackina returned valuesini veriyor
				this.callback(undefined, returnedValue);
				return;
			}

			this.execute();
			return;
		});
		line.execute();
	}

	static flowStandards(flowChange, returnedValue, senderCallback, callback) {
		if (flowChange === 'recall') {
			// recall geldi gönderenin de çıkması ve sonrakine de recall yapsın diye soylemesi lazım
			senderCallback(flowChange, returnedValue);
			return;
		}

		callback(flowChange, returnedValue);
	}
}

/**
 * Class representing TiLang Executer
 */
module.exports = class Executer {
	/**
	 * @param  {Object}   opts               [optional] an Object containing options.
	 * @param  {String}   opts.variableSign  [optional] variable sign used in script
	 * @param  {RegExp}   opts.variableRegex [optional] variable Regex
	 * @param  {Boolean}  opts.abortOnError  [optional] whether or not to abort on errors
	 * @param  {Function} opts.endCallback   [optional] callback to run when everything has finished
	 * @param  {Object}   opts.methods       [optional] custom methods for executer to execute, methods become an extra callback argument they need to execute.
	 * @param  {Array}    opts.global        [optional] custom variables to inject in executers runtime                __                             ___            _aaaa
               d8888aa,_                    a8888888a   __a88888888b
              d8P   `Y88ba.                a8P'~~~~Y88a888P""~~~~Y88b
             d8P      ~"Y88a____aaaaa_____a8P        888          Y88
            d8P          ~Y88"8~~~~~~~88888P          88g          88
           d8P                           88      ____ _88y__       88b
           88                           a88    _a88~8888"8M88a_____888
           88                           88P    88  a8"'     `888888888b_
          a8P                           88     88 a88         88b     Y8,
           8b                           88      8888P         388      88b
          a88a                          Y8b       88L         8888.    88P
         a8P                             Y8_     _888       _a8P 88   a88
        _8P                               ~Y88a888~888g_   a888yg8'  a88'
        88                                   ~~~~    ~""8888        a88P
       d8'                                                Y8,      888L
       8E                                                  88a___a8"888
      d8P                                                   ~Y888"   88L
      88                                                      ~~      88
      88                                                              88
      88                                                              88b
  ____88a_.      a8a                                                __881
88""P~888        888b                                 __          8888888888
      888        888P                                d88b             88
     _888ba       ~            aaaa.                 8888            d8P
 a888~"Y88                    888888                 "8P          8aa888_
        Y8b                   Y888P"                                88""888a
        _88g8                  ~~~                                 a88    ~~
    __a8"888_                                                  a_ a88
   88"'    "88g                                                 "888g_
   ~         `88a_                                            _a88'"Y88gg,
                "888aa_.                                   _a88"'      ~88
                   ~~""8888aaa______                ____a888P'
                           ~~""""""888888888888888888""~~~
                                      ~~~~~~~~~~~~

	 */
	constructor(opts) {
		opts = opts || {};

		this.variableSign = opts.variableSign || '$';
		this.variableRegex = opts.variableRegex || /\$\{[A-Z0-9a-z_]*\}/g;

		this.abortOnError = opts.abortOnError || false;
		this.endCallback = opts.endCallback || noop;
		this.customFunctions = opts.methods || {};
		this.globalScope = opts.global || [{}];
		// convert primitive JS variable
		for (let key in this.globalScope[0]) { // eslint-disable-line guard-for-in
			Utils.saveToScope(true, key, Executer.createVariable(this.globalScope[0][key]), this.globalScope);
			delete this.globalScope[0][key];
		}

		this.sleepTime = 0;
	}

	/**
	 * @param  {Array}  commands an array of commands to execute
	 * @return {Object} this     the instance on which this method was called.
	 */
	execute(commands) {
		let main = new Block(this, commands, 'mainBlock', {superScope: this.globalScope}, this.endCallback);
		main.execute();

		return this;
	}

	/**
	 * @return {Object} built-in Functions and Statements of Executer
	 */
	builtInFunctions() {
		return {
			/* In these functions, 'this' is normally the returned object, which represents nothing
			 * but we are calling these functions with apply(this) inside Line, so there
			 * is no problem with them. Their this is an instance of Line.
			 *
			 * call cb after you are done
			 *
			 * if your function returns something you can pass it as a second argument of cb
			 * Do NOT pass anything into cb's first argument! pass undefined if you need to
			 * first argument is used as flow check
			 */
			delay(time, cb) {
				this.execRefer.sleepTime = time * 1000;
				cb();
			},

			print(text, cb) {
				console.log(text);
				cb();
			},

			_for(times, array, cb) {
				let counter = 1;
				let forCallback = (flowChange, returnedValue) => {
					Block.flowStandards(flowChange, returnedValue, cb, flowChange => {
						if (flowChange === 'jump') {
							// jump olmuş, for kendini kapatıyor.
							cb();
							return;
						}

						if (counter === times) {
							// for tamamlandı
							cb();
							return;
						}

						counter++;
						let blockn = new Block(this.execRefer, deepCopy(array), 'forBlock', {superScope: this.scope}, forCallback);
						blockn.execute();
						return;
					});
				};
				let block = new Block(this.execRefer, deepCopy(array), 'forBlock', {superScope: this.scope}, forCallback);
				block.execute();
			},

			_if(con1, operator, con2, array, cb) {
				let doit = () => {
					let block = new Block(this.execRefer, deepCopy(array), 'ifBlock', {superScope: this.scope}, (flowChange, returnedValue) => {
						Block.flowStandards(flowChange, returnedValue, cb, flowChange => {
							cb(flowChange);
							return;
						});
					});
					block.execute();
				};

				if (operator === '=') {
					if (con1 === con2) {
						doit();
						return;
					}
				}

				if (operator === '>') {
					if (con1 > con2) {
						doit();
						return;
					}
				}

				if (operator === '<') {
					if (con1 < con2) {
						doit();
						return;
					}
				}

				if (operator === '<=') {
					if (con1 <= con2) {
						doit();
						return;
					}
				}

				if (operator === '>=') {
					if (con1 >= con2) {
						doit();
						return;
					}
				}
				cb();
			},

			_func() {
				// params be like:
				// name, param1, param2, param3, ..., paramN, array, cb
				let args = Array.prototype.slice.call(arguments);
				let name = args.shift();
				let cb = args.pop();
				let array = args.pop();
				let params = args;

				// eğer isim boşsa (anonymous func)
				if (name === '') {
					cb(undefined, Utils.createFunc(params, array));
					return;
				}

				this._setFunc(name, params, array);
				cb(undefined, this._getFunc(name));
			},

			_loneScope(array, cb) {
				let block = new Block(this.execRefer, deepCopy(array), 'lonelyBlock', {superScope: this.scope}, (flowChange, returnedValue) => {
					Block.flowStandards(flowChange, returnedValue, cb, flowChange => {
						cb(flowChange);
						return;
					});
				});
				block.execute();
			},

			date(cb) {
				cb(undefined, Date.now());
			},

			math(expr, cb) {
				let answer = mathjs.eval(expr);
				cb(undefined, answer);
			},

			assign(variable, value, cb) {
				// variable will come as A and will be saved as $A
				// eğer hiç bi yerde yoksa globale yazsın
				this._setVariable(false, variable, value);
				cb();
			},

			set(variable, value, cb) {
				// let var gibi define eder
				// variable will come as A and will be saved as $A
				this._setVariable(true, variable, value);
				cb();
			},

			// continue (on function bodies the same of recall) (can be used in to jump/skip [they are the same thing when there is only 1 turn])
			skip(cb) {
				cb('skip');
			},

			// break (on function bodies the same of recall) (can be used in to jump/skip [they are the same thing when there is only 1 turn])
			jump(cb) {
				cb('jump');
			},

			// return
			recall(returnedValue, cb) {
				if (typeof returnedValue === 'function') {
					cb = returnedValue;
					returnedValue = '';
				}

				cb('recall', returnedValue);
			}
		};
	}

	static createVariable(variable) {
		return Utils.createVariable(variable);
	}
};
