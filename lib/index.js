'use strict';
const Parser = require('./parser');
const watchr = require('watchr');
const fs = require('fs');
const Executer = require('./executer');

module.exports = class TiLang {
	/**
	 * @param  {Object}   opts               [optional] an Object containing options.
	 * @param  {String}   opts.variableSign  [optional] variable sign used in script
	 * @param  {RegExp}   opts.variableRegex [optional] variable Regex
	 * @param  {Boolean}  opts.abortOnError  [optional] whether or not to abort on errors
	 * @param  {Function} opts.endCallback   [optional] callback to run when everything has finished
	 * @param  {Object}   opts.methods       [optional] custom methods for executer to execute, methods become an extra callback argument they need to execute.
	 */
	constructor(opts) {
		this.executer = new Executer(opts);
		this.parser = new Parser(opts);
		this.builtIns = this.executer.builtInFunctions();
	}

	run(str) {
		var command = this.parser.parse(str);
		this.executer.execute(command);

		return this;
	}

	watch(watchPath) {
		watchr.watch({
			path: watchPath,
			listener: (type, path) => {
				this.run(fs.readFileSync(path, 'utf8'));
			}
		});

		return this;
	}

	parse(str) {
		return this.parser.parse(str);
	}
};
