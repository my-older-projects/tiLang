'use strict';
// TiLang Parser

// Constants

const seperator = ';';
const argumentSeperator = '|';
const opsSeperator = '|';
const bds = {
	start: '{',
	end: '}',
	sReg: /\{/,
	eReg: /\}$/
};
const cmnt = {
	start: '/*',
	end: '*/',
	regex: /\/\*[\s\S]*\*\//g
};
const subCom = {
	start: '(',
	end: ')',
	sReg: /\(/,
	eReg: /\)$/
};

// Utilities
const unescapeHTML = escapedHTML => escapedHTML.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');

module.exports = class Parser {
	constructor() {}

	parse(text) {
		text = unescapeHTML(text);
		// remove all comments
		text = text.replace(cmnt.regex, '').replace(/#.*?\n/g, '');
		let rawLines = splitFormedCodeString(text);
		let processedLines = classifyActions(rawLines);
		let commands = postProcessProcessedLines(processedLines);
		// console.dir(commands, {depth: null})

		return commands;
	}
};

function splitFormedCodeString(string) {
	let array = seperateBasic(string);
	// remove the ;'s at he end
	for (let i = 0; i < array.length; i++) {
		array[i] = array[i].replace(/;$/, '');
	}
	return array;
}

function seperateBasic(string) {
	var add = function(ch, indis) {
		if (typeof array[indis] === 'undefined') {
			array[indis] = '';
		}
		array[indis] = array[indis] + ch;
	}
	var countIndis = function(indis) {
		if (!openParantez) {
			if (array[indis] === seperator) { // eğer array boşsa
				array.splice(indis,1);
			} else {
				indis++;
			}
		}
		return indis;
	}
	var array = [];
	var indis = 0;
	var openParantez = 0;
	let lastWasDollar = false;
	let inAString = false;
	for (var i = 0; i < string.length; i++) {  //For every Char
		var ch = string[i];

		if (inAString) {
			if (ch === inAString) {
				inAString = false;
				add(ch, indis);
			} else {
				add(ch, indis);
			}
		} else {
			if (ch === bds.start || ch === subCom.start) { // added subCom check
				openParantez++;
				add(ch, indis);
			} else if (ch === bds.end) {
				openParantez--;
				add(ch, indis);
				if (lastWasDollar) {
					lastWasDollar = false;
				} else {
					indis = countIndis(indis);
				}
			} else if (ch === subCom.end) { // added subCom check
				openParantez--;
				add(ch, indis);
			} else if (ch === seperator) {
				add(ch, indis);
				indis = countIndis(indis);
			} else if (ch === '$') {
				lastWasDollar = true;
				add(ch, indis);
			} else if (/\s/.test(ch)) {

			} else if (ch === "'" || ch === '"') {
				inAString = ch;
				add(ch, indis);
			} else {
				add(ch, indis);
			}
		}
	}
	return array;
}

function seperateArgs(string) {
	let array = [];
	let indis = 0;
	let inAString = false;
	let openParantez = 0;
	let add = function(ch, indis) {
		if (typeof array[indis] === 'undefined') {
			array[indis] = '';
		}
		array[indis] += ch;
	};
	// for every char
	for (let i = 0; i < string.length; i++) {
		if (inAString) {
			if (string.charAt(i) === inAString) {
				inAString = false;
				add(string.charAt(i), indis);
			} else {
				add(string.charAt(i), indis);
			}
		} else {
			if ((string.charAt(i) === argumentSeperator || string.charAt(i) === opsSeperator) && !openParantez) {
				indis++;
				continue;
			}

			if (string.charAt(i) === subCom.start) {
				openParantez++;
				add(string.charAt(i), indis);
				continue;
			}

			if (string.charAt(i) === subCom.end) {
				openParantez--;
				add(string.charAt(i), indis);
				continue;
			}

			if (string.charAt(i) === "'" || string.charAt(i) === '"') {
				inAString = string.charAt(i);
				add(string.charAt(i), indis);
				continue;
			}

			add(string.charAt(i), indis);
		}
	}

	return array;
}

function classifyActions(parsedArray) {
	let processedArray = [];
	// For every line
	for (let i = 0; i < parsedArray.length; i++) {
		let proLine = processedArray[i] = {};
		let line = parsedArray[i];
		// bundles
		if (line.charAt(0) === bds.start) {
			proLine = solveBundles(line);
		} else {
			proLine.COMMAND = seperateArgs(line)[0];
			proLine.ARGS = seperateArgs(line).slice(1);
			proLine.ARGS = parseSubCommandsInARGS(proLine.ARGS);
		}
		// BUGFIX: proLine wasnt referencing processedArray[i]
		// Put them back
		processedArray[i] = proLine;
	}

	return processedArray;
}

function parseSubCommandsInARGS(argsArray) {
	// sıralı argüman stringleri gelir, sub commandları bitimiş olarak çıkarlar
	for (let i = 0; i < argsArray.length; i++) {
		// for every arg
		let arg = argsArray[i];
		// eğer boş argsa atla
		if (typeof arg === 'undefined') {
			argsArray[i] = '';
			continue;
		}
		// subcomları parsele
		if (arg.indexOf(subCom.start) + 1) {
			let openParantez = 0;
			let array = [''];
			let indis = 0;
			let inAString = false;
			let add = function(ch, indis) {
				if (typeof array[indis] === 'undefined') {
					array[indis] = '';
				}
				array[indis] += ch;
			};

			let countIndis = function(indis) {
				if (array[indis] === '') {
					array.splice(indis, 1);
				} else {
					indis++;
				}
				return indis;
			};

			// for every char
			for (let i = 0; i < arg.length; i++) {
				if (inAString) {
					if (arg.charAt(i) === inAString) {
						inAString = false;
						add(arg.charAt(i), indis);
					} else {
						add(arg.charAt(i), indis);
					}
				} else {
					if (arg.charAt(i) === subCom.start && !openParantez) {
						openParantez++;
						indis = countIndis(indis);
						add(arg.charAt(i), indis);
						continue;
					}

					if (arg.charAt(i) === subCom.start) {
						openParantez++;
						add(arg.charAt(i), indis);
						continue;
					}

					if (arg.charAt(i) === subCom.end && !(openParantez - 1)) {
						openParantez--;
						add(arg.charAt(i), indis);
						indis = countIndis(indis);
						continue;
					}

					if (arg.charAt(i) === subCom.end) {
						openParantez--;
						add(arg.charAt(i), indis);
						continue;
					}

					if (arg.charAt(i) === "'" || arg.charAt(i) === '"') {
						inAString = arg.charAt(i);
						add(arg.charAt(i), indis);
						continue;
					}

					add(arg.charAt(i), indis);
				}
			}

			for (let k = 0; k < array.length; k++) {
				if (array[k].charAt(0) === subCom.start && array[k].charAt(array[k].length - 1) === subCom.end) {
					array[k] = array[k].replace(subCom.eReg, '').replace(subCom.sReg, '');
					array[k] = splitFormedCodeString(array[k]);
					array[k] = classifyActions(array[k]);
					array[k] = postProcessProcessedLines(array[k]);

					array[k] = {
						SHELL: array[k]
					};
				}
			}

			if (array.length === 1 && typeof array[0] === 'string') {
				// eğer false pozitif verdiysek
				arg = array[0];
			} else {
				arg = {
					SHELLS: array
				};
			}
		}
		argsArray[i] = arg;
	}

	return argsArray;
}

function solveBundles(bundleString) {
	// remove ( )
	bundleString = bundleString.replace(bds.eReg, '').replace(bds.sReg, '');
	// add ; to end if there is not
	if (bundleString.charAt(bundleString.length - 1) !== seperator) {
		bundleString = bundleString.concat(seperator);
	}
	let parsedArray = splitFormedCodeString(bundleString);
	let processedArray = classifyActions(parsedArray);
	return processedArray;
}

function postProcessProcessedLines(arr) {
	for (let i = 0; i < arr.length; i++) {
		let indis = arr[i];
		if (indis.COMMAND === 'if' || indis.COMMAND === 'for' || indis.COMMAND === 'func') {
			/* Plan:
			 * 1) Bi aşşağsını (bundle) çıkar ve sakla
			 * 2) Bi aşşağsını tek başına `postProcess'e` (bu fonksiyon) recursive olcak olarak sok
			 * 3) Bu satıra komut olarak func argüman olarak postProcess den dönen şeyi koy.
			 */
			arr.splice(i, 1);
			let bundle = arr.splice(i, 1)[0];
			let command = '_'.concat(indis.COMMAND);
			let pack = [postProcessProcessedLines(bundle)];
			arr.splice(i, 0, {COMMAND: command, ARGS: indis.ARGS, PACK: pack});
			i--;
		} else if (Array.isArray(indis)) {
			// lonely bundle, scope
			let bundle = arr.splice(i, 1)[0];
			let pack = [postProcessProcessedLines(bundle)];
			arr.splice(i, 0, {COMMAND: '_loneScope', PACK: pack});
			i--;
		}
	}

	return arr;
}
