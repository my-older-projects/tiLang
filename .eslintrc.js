module.exports = {
	rules: {
		'indent': [
			2,
			'tab',
			{SwitchCase: 1}
		],
		'no-unused-expressions': [
			2,
			{allowShortCircuit: true}
		],
		'quotes': [
			2,
			'single',
			'avoid-escape'
		]
	},
	extends: 'google'
};
