'use strict';
const TiLang = require('../');

let tiLangInstance = new TiLang({
	global: [{
		A_GLOBALVARIABLE: 'node.js',
		INFINITY: Infinity
	}],
	methods: {
		sayHi(cb) {
			console.log('Hii');
			return cb();
		},
		sayWhatISaid(text, cb) {
			console.log('You said: ', text);
			return cb();
		},
		multiply(multiplier1, multiplier2, cb) {
			return cb(undefined, multiplier1 * multiplier2);
		}
	}
});

tiLangInstance.watch('tiFiles/test.ti');
