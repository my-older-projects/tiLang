/* eslint no-use-before-define: [2, "nofunc"] */
'use strict';
class Elma {
	constructor() {}

	tellName() {
		return 'Elma';
	}

	sayHiWithFriends() {
		let friend = new Armut();
		console.log(this.tellName(), 'and', friend.tellName());
	}
}

class Armut {
	constructor() {}

	tellName() {
		return 'Armut';
	}

	sayHiWithFriends() {
		let friend = new Elma();
		console.log(this.tellName(), 'and', friend.tellName());
	}
}

let e = new Elma();
e.sayHiWithFriends();

let a = new Armut();
a.sayHiWithFriends();
