# ti-lang [![NPM version][npm-image]][npm-url]
> tiLang is a Language which compiles into JSON and executes with Javascript!

## Installation

```sh
$ npm install --save ti-lang
```

## Usage


```js
var tiLang = require('ti-lang');

let lang = new tiLang({
	global: [{
		A_GLOBAL_VARIABLE: 'node.js'
	}],
	methods: {
		sayHi(cb) {
			console.log('Hii');
			return cb();
		},
		sayWhatISaid(text, cb) {
			console.log('Yo said: ', text)
			return cb();
		},
		multiply(multiplier1, multiplier2, cb) {
			return cb(undefined, multiplier1 * multiplier2);
		}
	}
});

lang.execute('sayHi; set|num|15; print|${num};');
```

You can find more detailed usage examples in examples/tiFiles/test.ti.
## License

Apache-2.0 © [omeryagmurlu](https://omeryagmurlu.github.com)


[npm-image]: https://badge.fury.io/js/ti-lang.svg
[npm-url]: https://www.npmjs.com/package/ti-lang
